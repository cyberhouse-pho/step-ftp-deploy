#!/bin/bash

# confirm environment variables
if [ ! -n "$WERCKER_NEW_FTP_DEPLOY_DESTINATION" ]
then
    fail "missing option \"destination\", aborting"
fi
if [ ! -n "$WERCKER_NEW_FTP_DEPLOY_USERNAME" ]
then
    fail "missing option \"username\", aborting"
fi
if [ ! -n "$WERCKER_NEW_FTP_DEPLOY_PASSWORD" ]
then
    fail "missing option \"password\", aborting"
fi

php $WERCKER_STEP_ROOT/ftp-deploy.php

success "done."
