<?php
(getenv ( "ERROR_REPORTING" ) !== false) ? null : error_reporting ( 0 );
date_default_timezone_set ( 'America/Los_Angeles' );
ini_set ( 'max_execution_time', '300' );
ini_set ( 'memory_limit', '128M' );

$globalStart = time ();
$werckerTimeout = 25;

$ftp_url = parse_url ( getenv ( "WERCKER_NEW_FTP_DEPLOY_DESTINATION" ) );
$ftp_server = $ftp_url ['host'];
$ftp_path = '.' . preg_replace ( '/((\\\\+)|(\/+))+$/', '', $ftp_url ['path'] );
$ftp_user_name = getenv ( "WERCKER_NEW_FTP_DEPLOY_USERNAME" );
$ftp_user_pass = getenv ( "WERCKER_NEW_FTP_DEPLOY_PASSWORD" );

$remoteFilepath = getenv ( "WERCKER_NEW_FTP_DEPLOY_REMOTE_FILE" );
if (! (0 < strlen ( $remoteFilepath ))) {
	$remoteFilepath = './remote.txt';
}

$cacheDir = getenv ( "WERCKER_CACHE_DIR" );
$cacheFilepath = "$cacheDir/remote.txt";

$skipPatterns = explode ( ',', getenv ( "WERCKER_NEW_FTP_DEPLOY_EXCLUDE_PATTERNS" ) );
if (! is_array ( $skipPatterns )) {
	if (! empty ( $argv [6] )) {
		echo "\n\nERROR:\n" . error_get_last ()['message'];
		echo "\nPlease check your skipPatterns variable for";
		echo " correctness or omit it if not needed.";
		exit ( - 1 );
	}
	$skipPatterns = array ();
}

// true -> skip
// false -> sync
// filters against skipPatterns wilcard patterns
function skipFilter($path) {
	global $skipPatterns;
	foreach ( $skipPatterns as $pattern ) {
		if (fnmatch ( $pattern, $path )) {
			if (is_dir ( $path )) {
				$skipPatterns [] = $path . "/*";
			}
			return true;
		}
	}
	return false;
}

// checks if we have to abort and continue in the next run due to wercker timeout
function timeoutNears() {
	global $globalStart;
	global $werckerTimeout;
	$globalDurance = (time () - $globalStart) / 60;
	return ! ($globalDurance < ($werckerTimeout - 5));
}
function echoProgress($i, $sum, $start) {
	if ($i == 1) {
		ob_start ();
	}
	$ppm = (1000000 * $i) / $sum;
	$durance = time () - $start;
	echo sprintf ( "%05.2f", $ppm/10000 ) . "% - approx. " . date ( "i\m s\s", ((1000000 * $durance) / $ppm) - $durance ) . " left\r";
	ob_flush ();
	if ($i == $sum) {
		ob_end_flush ();
	}
}

// Verbindung aufbauen
$conn_id = ftp_connect ( $ftp_server );

if (! $conn_id) {
	echo "\n\nERROR:\n" . error_get_last ()['message'];
	echo "\nPlease provide the ftp url as ftp://your.url.com/";
	echo " in the destination variable.";
	exit ( - 1 );
}

// Login mit Benutzername und Passwort
$login_result = ftp_login ( $conn_id, $ftp_user_name, $ftp_user_pass );

if (! $login_result) {
	echo "\n\nERROR:\n" . error_get_last ()['message'];
	echo "\nPlease provide the correct login credentials as";
	echo " username and password variable.";
	exit ( - 1 );
}

// Schalte passiven Modus ein
ftp_pasv ( $conn_id, true );

// delete cached file
if (file_exists ( $cacheFilepath )) {
	unlink ( $cacheFilepath );
}
// download remote control file
if (ftp_size ( $conn_id, $remoteFilepath ) != - 1) {
	if (! ftp_get ( $conn_id, $cacheFilepath, $remoteFilepath, FTP_ASCII )) {
		echo "\n\nERROR:\n" . error_get_last ()['message'];
		echo "\nWas not able to get remote control file ($remoteFilepath).";
		echo " There may be a rights problem or an other error preventing";
		echo " readability.";
		exit ( - 1 );
	}
} else {
	// test if control file is writeable
	file_put_contents ( $cacheFilepath, "[ ]" );
	if (! ftp_put ( $conn_id, $remoteFilepath, $cacheFilepath, FTP_ASCII )) {
		echo "\n\nERROR:\n" . error_get_last ()['message'];
		echo "\nWas not able to write the control file ($remoteFilepath).";
		echo " There may be a rights problem or an other error preventing";
		echo " writeability.";
		exit ( - 1 );
	}
}

$remote = array ();
if (file_exists ( $cacheFilepath )) {
	$remote = json_decode ( file_get_contents ( $cacheFilepath ), true );
}
if (! is_array ( $remote )) {
	$remot = array ();
}
// call array_filter twice cause additional patterns are added at first run
array_filter ( array_keys ( $remote ), "skipFilter" );
$remote = array_diff_key ( $remote, array_flip ( array_filter ( array_keys ( $remote ), "skipFilter" ) ) );

$root = '.';
$iterator = new RecursiveIteratorIterator ( new RecursiveDirectoryIterator ( $root, FilesystemIterator::UNIX_PATHS + RecursiveDirectoryIterator::SKIP_DOTS ), RecursiveIteratorIterator::CHILD_FIRST, RecursiveIteratorIterator::CATCH_GET_CHILD ); // Ignore "Permission denied"

$local = array ();
foreach ( $iterator as $path => $dir ) {
	if (is_file($path)) {
		$local [$path] = md5_file ( $path );
	} else {
		$local [$path] = 1;
	}
}
// call array_filter twice cause additional patterns are added at first run
array_filter ( array_keys ( $local ), "skipFilter" );
$local = array_diff_key ( $local, array_flip ( array_filter ( array_keys ( $local ), "skipFilter" ) ) );

$new = array_diff_key ( $local, $remote );
ksort ( $new, SORT_STRING ); // create folders first

$deleted = array_diff_key ( $remote, $local );
krsort ( $deleted, SORT_STRING ); // delete files first

$same = array_intersect_key ( $local, $remote );
$changed = array_merge ( array_diff_assoc ( $same, $local ), array_diff_assoc ( $same, $remote ) );

if (! timeoutNears ()) {
	$i = 1;
	$sum = count ( $new );
	$start = time ();
	echo "\nUpload New ($sum file(s))\n";
	foreach ( $new as $path => $md5 ) {
		echoProgress ( $i ++, $sum, $start );
		// change on ftp
		if ($md5 === 1) { // 1 if is dir
			ftp_mkdir ( $conn_id, preg_replace ( '/^\.(\/.*)/', $ftp_path . '$1', $path ) );
		} else {
			ftp_put ( $conn_id, preg_replace ( '/^\.(\/.*)/', $ftp_path . '$1', $path ), $path, FTP_BINARY );
		}
		// change on array
		$remote [$path] = $md5;
		if (timeoutNears ()) {
			break;
		}
	}
	// add some blanks here to override approx. time
	echo "                              \r";
	// list files
	if (count($new) <= 100) {
		print_r(array_keys($new));
	}
	echo "\n";
}

if (! timeoutNears ()) {
	$i = 1;
	$sum = count ( $changed );
	$start = time ();
	echo "Replace changed ($sum file(s))\n";
	foreach ( $changed as $path => $md5 ) {
		echoProgress ( $i ++, $sum, $start );
		// change on ftp
		ftp_put ( $conn_id, preg_replace ( '/^\.(\/.*)/', $ftp_path . '$1', $path ), $path, FTP_BINARY );
		// change on array
		$remote [$path] = $md5;
		if (timeoutNears ()) {
			break;
		}
	}
	// add some blanks here to override approx. time
	echo "                              \r";
	// list files
	if (count($changed) <= 100) {
		print_r(array_keys($changed));
	}
	echo "\n";
}

if (! timeoutNears ()) {
	$i = 1;
	$sum = count ( $deleted );
	$start = time ();
	echo "Delete removed ($sum file(s))\n";
	foreach ( $deleted as $path => $md5 ) {
		echoProgress ( $i ++, $sum, $start );
		// change on ftp
		if ($md5 === 1) { // 1 if is dir
			ftp_rmdir ( $conn_id, preg_replace ( '/^\.(\/.*)/', $ftp_path . '$1', $path ) );
		} else {
			ftp_delete ( $conn_id, preg_replace ( '/^\.(\/.*)/', $ftp_path . '$1', $path ) );
		}
		// change on array
		unset ( $remote [$path] );
		if (timeoutNears ()) {
			break;
		}
	}
	// add some blanks here to override approx. time
	echo "                              \r";
	// list files
	if (count($changed) <= 100) {
		print_r(array_keys($deleted));
	}
	echo "\n";
}

file_put_contents ( $cacheFilepath, json_encode ( $remote ) );
ftp_put ( $conn_id, $remoteFilepath, $cacheFilepath, FTP_ASCII );

// close connection
ftp_close ( $conn_id );

// check if process exited early cause of near timeout
ksort ( $local );
ksort ( $remote );
if ($local !== $remote) {
	echo "SCRIPT WAS STOPPED EARLY FOR NOT TO BE KILLED BY WERCKERS TIMEOUT LIMIT!\n";
	echo "THE CURRENT UPLOADING PROCESS WAS STORED AND WILL BE PROCEEDED NEXT RUN.\n";
	echo "PLEASE RESTART THE SCRIPT IMMEDIATLY FOR THE UPLOAD TO BE COMPLETED!";
	exit ( - 1 );
}
