# new-ftp-deploy

Deploy your files with FTP. It looks for list of files and their md5sum in **remote-file** and compare with local generated files. Any change of local files will be uploaded in this order:

1. Upload all new files
2. Replace all modified files
3. Remove all deleted files

If the order is different, active visitors of the website can see 404 pages because for example a modified file has an added menu item to a page which is not yet uploaded.
Another case is that a file is deleted before the files referring to it are updated.

# Options

* `destination` (required) Full FTP path where all files will be uploaded to. This path will not be applied to the remote-file though.
* `username` (required) Username to connect to FTP server.
* `password` (required) Password to connect to FTP server.
* `remote-file` (optional, default is a "./remote.txt") Control file which stores a fingerprint of every uploaded files. This is what makes syncing possible. Removing the file from the ftp server will cause a reset where on next run all files will be uploaded once again and a new remote-file will be generated.
* `exclude-patterns` (optional, default is "[ ]") Array with wildcard strings which match against the files which shant be considered in the synchronization process.

# Example

Add PASSWORD as protected environment variable. Other options can be hardcoded.

```yaml
deploy:
  steps:
    - bugscal/new-ftp-deploy:
        destination: ftp://domain.example.com/site/public_html
        username: ftpusername
        password: $PASSWORD
        remote-file: ./protected/werckerSync.txt
        exclude-patterns: [
            "./php",
            "./logs/*",
            "./.gitignore"
        ]
```

# Discussion

Comments on the step, feature requests and other discussions can be posted on [2081.onlineguestbook.de](http://2081.onlineguestbook.de/ "New FTP Deploy").

# Changelog

## 0.1.0

- Initial release
